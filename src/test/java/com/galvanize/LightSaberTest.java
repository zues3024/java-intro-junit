package com.galvanize;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LightSaberTest {

    @Test
    public void shouldReturnFloatInitialAmountOfCharge() {
        LightSaber check1 = new LightSaber(14L);
        float result = check1.getCharge();
        assertEquals(100.0f, result);
    }
    @Test
    public void shouldReturnNewAmountOfChargeAfterSettingCharge()
    {
        LightSaber check2 = new LightSaber(10L);
        check2.setCharge(99.9f);
        float result = check2.getCharge();
        assertEquals(99.9f, result);
    }

    @Test
    public void shouldReturnStringColor()
    {
        LightSaber check3 = new LightSaber(11L);
        String result = check3.getColor();
        assertEquals("green", result);
    }

    @Test
    public void shouldReturnNewStringColor()
    {
        LightSaber check4 = new LightSaber(12L);
        check4.setColor("Blue");
        String result = check4.getColor();
        assertEquals("Blue", result);
    }

    @Test
    public void shouldReturnLongTypeSerialNumberOfLightSaber()
    {
        long expected = 13L;
        LightSaber check5 = new LightSaber(expected);

        long result = check5.getJediSerialNumber();
        assertEquals(expected, result);
    }
    @Test
    public void shouldReturnRemainingMinutes()
    {
        LightSaber check6 = new LightSaber(16L);
        float expected = check6.getRemainingMinutes();
        float result = check6.getRemainingMinutes();
        assertEquals(expected,result);

    }
    @Test
    public void shouldUseLightSaberAnUpdateRemainingMinutes()
    {
        LightSaber check7 = new LightSaber(17L);
        check7.use(15f);
        float expected = check7.getRemainingMinutes();
        float result = check7.getRemainingMinutes();
        assertEquals(expected, result);
    }
    @Test
    public void foo()
    {
        LightSaber check8 = new LightSaber(18L);
        check8.recharge();
        float result = check8.getCharge();
        assertEquals(100.0f, result);
    }
}